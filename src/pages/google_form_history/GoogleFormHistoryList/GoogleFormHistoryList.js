import React from "react";
import {
  Datagrid,
  DateField,
  List,
  ReferenceField,
  TextField,
  useLocaleState
} from "react-admin";

const GoogleFormHistoryList = () => {
  const [locale] = useLocaleState();

  return (
    <List>
      <Datagrid>
        <TextField source="id" />
        <ReferenceField
          reference="users"
          source="userId"
        >
          <TextField source={`email`} />
        </ReferenceField>
        <ReferenceField
          reference="vacancies"
          source="vacancyId"
        >
          <TextField source={`name.${locale}`} />
        </ReferenceField>
        <DateField source={`date`} showTime={true} />
      </Datagrid>
    </List>
  );
};

export default GoogleFormHistoryList;
