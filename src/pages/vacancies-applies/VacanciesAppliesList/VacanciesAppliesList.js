import { Card, CardContent } from "@mui/material";
import Button from "@mui/material/Button";
import React from "react";
import {
  Datagrid,
  FilterForm,
  List,
  ReferenceField,
  ReferenceInput,
  TextField,
  useLocaleState,
  usePermissions,
  useRecordContext,
  useTranslate
} from "react-admin";
import { useNavigate } from "react-router-dom";

const filters = [
  <ReferenceInput
    label="resources.vacancies-applies.fields.schedule"
    source="vacancyId"
    reference="vacancies"
    alwaysOn
  />,
];

export const FilterSidebar = () => (
  <Card sx={{ order: -1, mr: 2, mt: 8, width: "300px" }}>
    <CardContent>
      <FilterForm filters={filters} />
    </CardContent>
  </Card>
);

const ChatButton = (props) => {
  const record = useRecordContext(props);
  const translate = useTranslate();
  const navigate = useNavigate();

  return record ? (
    <Button variant="text" style={{ float: "right" }} onClick={() => {
      navigate(`/chat/${record.id}`)
    }}>
      {translate("resources.chats.buttons.chat")}
    </Button>
  ) : null;
};

const VacanciesAppliesList = () => {
  const [locale] = useLocaleState();
  const { isLoading, permissions } = usePermissions();

  if (isLoading) return <div>Waiting for permissions...</div>;

  if (permissions === "ROLE_INTERVIEWER")
    return (
      <List aside={<FilterSidebar />} disableAuthentication>
        <Datagrid bulkActionButtons={false}>
          <TextField source="id" />
          <ReferenceField reference="vacancies" source="vacancyId">
            <TextField source={`name.${locale}`} />
          </ReferenceField>
          <ReferenceField reference="users" source="userId">
            <TextField source={`email`} />
          </ReferenceField>
          <ChatButton />
        </Datagrid>
      </List>
    );

  return (
    <List disableAuthentication>
      <Datagrid bulkActionButtons={false}>
        <TextField source="id" />
        <ReferenceField reference="vacancies" source="vacancyId">
          <TextField source={`name.${locale}`} />
        </ReferenceField>
        <ChatButton />
      </Datagrid>
    </List>
  );
};

export default VacanciesAppliesList;
