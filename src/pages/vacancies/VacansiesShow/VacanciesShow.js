import Button from "@mui/material/Button";
import * as React from "react";
import {
    Datagrid,
    DateField,
    NumberField,
    ReferenceField,
    ReferenceManyField,
    RichTextField,
    Show,
    SimpleShowLayout,
    TextField,
    useAuthState,
    useLocaleState,
    usePermissions,
    useRecordContext,
    useRefresh,
    useShowContext,
    useTranslate,
} from "react-admin";
import { useNavigate } from "react-router-dom";
import { axiosInstance } from "../../../dataProvider";

const SubscribeButton = (props) => {
  const refresh = useRefresh();
  const record = useRecordContext(props);
  const translate = useTranslate();
  const navigate = useNavigate();

  const subscribe = (id) => {
    axiosInstance
      .get(`/api/v1/interviews/${id}/subscribe`)
      .then((response) => {
        refresh();
        return response.data;
      })
      .catch((error) => {
        if (error.response.status === 401 || error.response.status === 403) {
          navigate("/login");
        }
      });
  };

  if (!record) return null;

  return (
    <Button
      variant="text"
      style={{ float: "right" }}
      onClick={() => subscribe(record.id)}
    >
      {translate("resources.interviews.buttons.subscribe")}
    </Button>
  );
};

const ApplyButton = (props) => {
  const refresh = useRefresh();
  const record = useRecordContext(props);
  const translate = useTranslate();
  const { authenticated } = useAuthState();
  const navigate = useNavigate();

  if (record.applied === true) {
    return (
      <div style={{ float: "right" }}>
        {translate("resources.vacancies.buttons.applied")}
      </div>
    );
  }

  const subscribe = (id) => {
    if (!authenticated) {
      navigate(`/login`);
      return;
    }

    axiosInstance.get(`/api/v1/vacancies/${id}/apply`).then((response) => {
      refresh();
      return response.data;
    });
  };

  return record ? (
    <Button
      variant="text"
      style={{ float: "right" }}
      onClick={() => subscribe(record.id)}
    >
      {translate("resources.vacancies.buttons.apply")}
    </Button>
  ) : null;
};

const FormButton = (props) => {
    const record = useRecordContext(props);
    const translate = useTranslate();
    const { authenticated } = useAuthState();
    const navigate = useNavigate();
  
    if (record.googleFormLink == null) {
      return null;
    }
  
    const subscribe = (id) => {
      if (!authenticated) {
        navigate(`/login`);
        return;
      }
  
      axiosInstance
        .post(`/api/v1/google-form-history`, {
          vacancyId: record.id,
        })
        .then((response) => {
          // @ts-ignore
          window.open(record.googleFormLink, "_blank").focus();
        });
    };
  
    return record ? (
      <Button
        variant="text"
        style={{ float: "right" }}
        onClick={() => subscribe(record.id)}
      >
        {translate("resources.vacancies.buttons.test")}
      </Button>
    ) : null;
  };

const Interviews = ({ permissions }) => {
  const context = useShowContext();

  if (context.isLoading) return <div>Waiting for data...</div>;

  return (
    <ReferenceManyField
      label="resources.vacancies.fields.interviews"
      reference="interviews"
      target="author_id"
      filter={{
        busyByUser: false,
        vacancyId: context.record.id,
      }}
    >
      <Datagrid bulkActionButtons={false}>
        <DateField source="start" showTime={true} />
        <ReferenceField reference="users" source="interviewerId">
          <>
            <TextField source="name" style={{ marginRight: "10px" }} />
            <TextField source="surname" />
          </>
        </ReferenceField>
        <SubscribeButton reference="id" permissions={permissions} />
      </Datagrid>
    </ReferenceManyField>
  );
};

const VacanciesShow = () => {
  const { isLoading, permissions } = usePermissions();
  const [locale] = useLocaleState();

  if (isLoading) return <div>Waiting for permissions...</div>;

  if (permissions === "ROLE_INTERVIEWER")
    return (
      <Show>
        <SimpleShowLayout>
          <TextField
            label="resources.vacancies.fields.name"
            source={`name.${locale}`}
          />
          <RichTextField
            label="resources.vacancies.fields.description"
            source={`description.${locale}`}
          />
          <ReferenceField source="scheduleId" reference="schedules">
            <TextField source={`name.${locale}`} />
          </ReferenceField>
          <ReferenceField source="typeEmploymentId" reference="type_employment">
            <TextField source={`name.${locale}`} />
          </ReferenceField>
          <NumberField source="salary" />
        </SimpleShowLayout>
      </Show>
    );

  return (
    <Show disableAuthentication>
      <SimpleShowLayout>
        <TextField
          label="resources.vacancies.fields.name"
          source={`name.${locale}`}
        />
        <RichTextField
          label="resources.vacancies.fields.description"
          source={`description.${locale}`}
        />
        <ReferenceField source="scheduleId" reference="schedules">
          <TextField source={`name.${locale}`} />
        </ReferenceField>
        <ReferenceField source="typeEmploymentId" reference="type_employment">
          <TextField source={`name.${locale}`} />
        </ReferenceField>
        <NumberField source="salary" />
        <Interviews permissions={permissions} />
        <FormButton />
        <ApplyButton />
      </SimpleShowLayout>
    </Show>
  );
};

export default VacanciesShow;
