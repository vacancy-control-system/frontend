import { Card, CardContent } from "@mui/material";
import Button from "@mui/material/Button";
import React from "react";
import {
    AutocompleteArrayInput,
    Datagrid,
    FilterForm,
    List,
    NumberField,
    ReferenceArrayInput,
    ReferenceField,
    ShowButton,
    TextField,
    useAuthState,
    useLocaleState,
    usePermissions,
    useRecordContext,
    useRefresh,
    useTranslate,
} from "react-admin";
import { useNavigate } from "react-router-dom";
import { axiosInstance } from "../../../dataProvider";

const postFilters = [
  <ReferenceArrayInput
    label="resources.vacancies.fields.schedule"
    source="schedulesIdIn"
    reference="schedules"
    alwaysOn
  >
    <AutocompleteArrayInput label="resources.vacancies.fields.schedule" />
  </ReferenceArrayInput>,
  <ReferenceArrayInput
    label="resources.vacancies.fields.typeEmployment"
    source="typeEmploymentIdIn"
    reference="type_employment"
    alwaysOn
  >
    <AutocompleteArrayInput label="resources.vacancies.fields.typeEmployment" />
  </ReferenceArrayInput>,
];

export const FilterSidebar = () => (
  <Card sx={{ order: -1, mr: 2, mt: 8, width: "300px" }}>
    <CardContent>
      <FilterForm filters={postFilters} />
    </CardContent>
  </Card>
);

const ApplyButton = (props) => {
  const refresh = useRefresh();
  const record = useRecordContext(props);
  const translate = useTranslate();
  const { authenticated } = useAuthState();
  const navigate = useNavigate();

  if (record.applied === true) {
    return (
      <div style={{ float: "right" }}>
        {translate("resources.vacancies.buttons.applied")}
      </div>
    );
  }

  const subscribe = (id) => {
    if (!authenticated) {
      navigate(`/login`);
      return;
    }

    axiosInstance.get(`/api/v1/vacancies/${id}/apply`).then((response) => {
      refresh();
      return response.data;
    });
  };

  return record ? (
    <Button
      variant="text"
      style={{ float: "right" }}
      onClick={() => subscribe(record.id)}
    >
      {translate("resources.vacancies.buttons.apply")}
    </Button>
  ) : null;
};

const FormButton = (props) => {
  const record = useRecordContext(props);
  const translate = useTranslate();
  const { authenticated } = useAuthState();
  const navigate = useNavigate();

  if (record.googleFormLink == null) {
    return null;
  }

  const subscribe = (id) => {
    if (!authenticated) {
      navigate(`/login`);
      return;
    }

    axiosInstance
      .post(`/api/v1/google-form-history`, {
        vacancyId: record.id,
      })
      .then((response) => {
        // @ts-ignore
        window.open(record.googleFormLink, "_blank").focus();
      });
  };

  return record ? (
    <Button
      variant="text"
      style={{ float: "right" }}
      onClick={() => subscribe(record.id)}
    >
      {translate("resources.vacancies.buttons.test")}
    </Button>
  ) : null;
};

const VacanciesList = () => {
  const [locale] = useLocaleState();
  const { isLoading, permissions } = usePermissions();

  if (isLoading) return <div>Waiting for permissions...</div>;

  if (permissions === "ROLE_ADMINISTRATOR")
    return (
      <List aside={<FilterSidebar />}>
        <Datagrid rowClick="edit">
          <TextField source="id" />
          <TextField
            label="resources.vacancies.fields.name"
            source={`name.${locale}`}
          />
          <ReferenceField
            label="resources.vacancies.fields.schedule"
            reference="schedules"
            source="scheduleId"
          >
            <TextField source={`name.${locale}`} />
          </ReferenceField>
          <ReferenceField
            label="resources.vacancies.fields.typeEmployment"
            reference="type_employment"
            source="typeEmploymentId"
          >
            <TextField source={`name.${locale}`} />
          </ReferenceField>
          <NumberField source="salary" />
        </Datagrid>
      </List>
    );

  if (permissions === "ROLE_DIRECTOR")
    return (
      <List aside={<FilterSidebar />}>
        <Datagrid rowClick="edit">
          <TextField source="id" />
          <TextField
            label="resources.vacancies.fields.name"
            source={`name.${locale}`}
          />
          <ReferenceField
            label="resources.vacancies.fields.schedule"
            reference="schedules"
            source="scheduleId"
          >
            <TextField source={`name.${locale}`} />
          </ReferenceField>
          <ReferenceField
            label="resources.vacancies.fields.typeEmployment"
            reference="type_employment"
            source="typeEmploymentId"
          >
            <TextField source={`name.${locale}`} />
          </ReferenceField>
          <NumberField source="salary" />
        </Datagrid>
      </List>
    );

  if (permissions === "ROLE_INTERVIEWER")
    return (
      <List aside={<FilterSidebar />}>
        <Datagrid rowClick="show" bulkActionButtons={false}>
          <TextField source="id" />
          <TextField
            label="resources.vacancies.fields.name"
            source={`name.${locale}`}
          />
          <ReferenceField
            label="resources.vacancies.fields.schedule"
            reference="schedules"
            source="scheduleId"
          >
            <TextField source={`name.${locale}`} />
          </ReferenceField>
          <ReferenceField
            label="resources.vacancies.fields.typeEmployment"
            reference="type_employment"
            source="typeEmploymentId"
          >
            <TextField source={`name.${locale}`} />
          </ReferenceField>
          <NumberField source="salary" />
        </Datagrid>
      </List>
    );

  return (
    <List aside={<FilterSidebar />} disableAuthentication>
      <Datagrid bulkActionButtons={false}>
        <TextField source="id" />
        <TextField
          label="resources.vacancies.fields.name"
          source={`name.${locale}`}
        />
        <ReferenceField
          label="resources.vacancies.fields.schedule"
          reference="schedules"
          source="scheduleId"
        >
          <TextField source={`name.${locale}`} />
        </ReferenceField>
        <ReferenceField
          label="resources.vacancies.fields.typeEmployment"
          reference="type_employment"
          source="typeEmploymentId"
        >
          <TextField source={`name.${locale}`} />
        </ReferenceField>
        <NumberField source="salary" />
        <ApplyButton />
        <FormButton />
        <ShowButton style={{ float: "right" }} />
      </Datagrid>
    </List>
  );
};

export default VacanciesList;
