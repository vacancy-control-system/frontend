import { Button, TextField } from "@mui/material";
import React, { useEffect, useRef, useState } from "react";
import { useTranslate, useAuthenticated } from "react-admin";
import { useParams } from "react-router-dom";
import SockJS from "sockjs-client";
import Stomp from "stomp-websocket";
import { axiosInstance } from "../../dataProvider";
import styles from "./Chat.module.css";
import Message from "./Message/Message";

const Chat = () => {
  useAuthenticated();
  const [messages, setMessages] = useState([]);
  const [userId, setUserId] = useState(null);
  const [stompClient, setStompClient] = useState(null);
  const translate = useTranslate();
  const { id } = useParams();
  const valueRef = useRef("");

  useEffect(() => {
    axiosInstance.get("/api/v1/auth/info").then((response) => {
      setUserId(response.data.id);
    });
    axiosInstance.get(`/api/v1/chats/${id}/messages`).then((response) => {
      console.log(response.data);
      setMessages(response.data);
    });
  }, [id]);

  useEffect(() => {
    if (stompClient != null) {
      return;
    }

    let socket = new SockJS("http://localhost:8080/api/v1/chat/websocket");
    let stompClientInit = Stomp.over(socket);

    setStompClient(stompClientInit);

    stompClientInit.connect(
      {
        Authorization: localStorage.getItem("token")
          ? "Bearer " + localStorage.getItem("token")
          : undefined,
      },
      function (frame) {
        stompClientInit.subscribe(
          `/events/chat/${id}/new-message`,
          function (message) {
            console.log(JSON.parse(message.body));
            // @ts-ignore
            setMessages((messages) => [...messages, JSON.parse(message.body)]);
          }
        );
      }
    );
  }, [id]);

  const submit = () => {
    // @ts-ignore
    stompClient.send(
      `/action/chat/${id}/send-message`,
      {
        Authorization: localStorage.getItem("token")
          ? "Bearer " + localStorage.getItem("token")
          : undefined,
      },
      // @ts-ignore
      valueRef.current.value
    );
    // @ts-ignore
    valueRef.current.value = null;
  };

  return (
    <div className={styles.Container}>
      <div>
        <TextField
          id="outlined-multiline-static"
          label={translate('chat.fields.text')}
          multiline
          fullWidth
          inputRef={valueRef}
        />
        <Button variant="contained" style={{ float: "right" }} onClick={submit}>
          {translate('chat.buttons.send')}
        </Button>
      </div>
      <div className={styles.MessagesWrapper}>
        <div className={styles.Messages}>
          {messages.map((item, i) => (
            <Message
              key={i}
              // @ts-ignore
              userName={item.username}
              // @ts-ignore
              text={item.text}
              // @ts-ignore
              isMe={userId === item?.userId}
            />
          ))}
        </div>
      </div>
    </div>
  );
};

export default Chat;
