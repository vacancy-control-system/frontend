import { useLocaleState } from "react-admin";
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import React from "react";

const Message = ({ userName, isMe, text }) => {
  const [locale] = useLocaleState();

  let style = { minWidth: 275, maxWidth: "70%", marginBottom: "10px", width: "fit-content" }

  if (isMe) {
    style["margin-left"] = "auto"
  }

  return (
    <Card sx={style}>
      <CardContent>
        <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
          {userName}
        </Typography>
        <Typography variant="body2">
          {text}
        </Typography>
      </CardContent>
    </Card>
  );
};

export default Message;
